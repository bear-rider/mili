import math
import os
import random
import re
import sys

def calculateTip(meal_cost, tip_percent):
    tip = meal_cost * (tip_percent/100)
    return tip

def calculateTax(meal_cost, tax_percent):
    tax = meal_cost * (tax_percent/100)
    return tax

def sum(mc, tp, tx):
    sum = tp + tx + mc
    return sum

# Complete the solve function below.
def solve(meal_cost, tip_percent, tax_percent):
    tip = calculateTip(meal_cost, tip_percent)
    tax = calculateTax(meal_cost, tax_percent)
    totalcost = sum(tip, tax, meal_cost)
    return (totalcost)


if __name__ == '__main__':
    meal_cost = float(input())

    tip_percent = int(input())

    tax_percent = int(input())

    cost =  solve(meal_cost, tip_percent, tax_percent)
    print(round(cost))
