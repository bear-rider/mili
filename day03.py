#!/bin/python3

import math
import os
import random
import re
import sys

def check_weirdness(n):
    if (n%2 != 0):
        return "Weird"
    elif (n%2 == 0) and (n >= 2) and (n <= 5):
        return  "Not Weird"
    elif(n%2 == 0) and (n >= 6) and (n <=20):
        return "Weird"
    elif(n%2 == 0) and (n > 20):
        return "Not Weird"
    else:
        return "Not Weird"


if __name__ == '__main__':
    N = int(input())
    weirdness = check_weirdness(N)
    print(weirdness)
